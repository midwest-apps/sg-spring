package hello;

import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestMapping;

@RestController
public class HelloController {

    @RequestMapping("/")
    public String index() {
        String style = "<style type='text/css' media='screen'>";
        int placeholder;

        style += "body { background-color:Aquamarine; position: fixed; top: 50%; left: 50%; transform: translate(-50%, -50%); color: BlueViolet; font-size: 250%; font-family: monospace; font-name: OCR A Std; font-style: normal; }";
        style += "</style>";
        
        String message = "Support those women you know who are innovators and entrepreneurs on Women's Entrepreneurship Day November 19.";

// introduce static vulnerability
        // Introduce a static vulnerability by using user input directly in the response
        String body = "<body>" + message + "</body>";
        String userInput = System.getProperty("user.input", "");
        body = "<body>" + message + userInput + "</body>";

        return style + body;
    }

}


